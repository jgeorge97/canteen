// Your web app's Firebase configuration
var firebaseConfig = {
//Add Firebase config here
    };
    // Initialize Firebase
firebase.initializeApp(firebaseConfig);

userRef = firebase.database().ref('users');
function signUp(){
    console.log("start");
    var email = $("#Email").val();
    var password = $("#Password").val();
    firebase.auth().createUserWithEmailAndPassword(email, password)
	.then(function(){
        addUserDetails();
		location.href="dash.html";
	})
	.catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        $("#error").html(errorMessage);
      });
}

function addUserDetails() {
    var email = $("#Email").val();
    var mob = $("#mobile").val();
    var name = $("#Name").val();
    var rid = $("#rfid").val();
    userRef.push().set({
        email:email,
        mobile:mob,
        name:name,
        rfid:rid
    })
}

$('#signup-form').on("submit",function(e) {
    e.preventDefault(); // cancel the actual submit
	signUp();
});
