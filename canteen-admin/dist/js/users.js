// Your web app's Firebase configuration
var firebaseConfig = {
//Add Firebase config here
    };
    // Initialize Firebase
firebase.initializeApp(firebaseConfig);

var session=firebase.auth();

function out(){
    session.signOut().then(function() {
        // Sign-out successful.
        window.location='index.html';
    }).catch(function(error) {
        // An error happened.
        alert(error.message);
    });
    return true;
}

session.onAuthStateChanged(function(user) {
    if (!user) {
      window.location='index.html';
    }
});

var dbRef = firebase.database().ref();
      dbRef.child('users').once('value', function(snapshot){
        if(snapshot.exists()){
            var content = '';
            snapshot.forEach(function(data){
                var v = data.val();
                content +='<tr>';
                content += '<td>' + v.name + '</td>';
                content += '<td>' + v.email + '</td>';
                content += '<td>' + v.mobile + '</td>';
                content += '<td>' + v.rfid + '</td>';
                content += '</tr>';         
            });
            $('#ex-table tbody').append(content);
            $('#ex-table').DataTable();

        }});

function reset(){
    var x = confirm("Do you want to Reset all orders? This action cannot be undone.");
    if (x==true) {
    dbRef.child('orders').remove();
    window.location.reload();
    }
}
