// Your web app's Firebase configuration
var firebaseConfig = {
//Add Firebase config here
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
var session=firebase.auth();

function out(){
    session.signOut().then(function() {
        // Sign-out successful.
        window.location='index.html';
    }).catch(function(error) {
        // An error happened.
        alert(error.message);
    });
    return true;
}

session.onAuthStateChanged(function(user) {
    if (!user) {
      window.location='index.html';
    }
});

function loadModule(module){
    var url = "modules/"+module+".html";
    $.get(url,function(data){
        $('#main').html(data);
    });
  }

$(window).on("load",function(){
    $(".navbar-nav li").on("click",function(){
      loadModule($(this).data("target"));
      $(".navbar-nav li").removeClass("active");
      $("#content").html("<section class='content'><center><div class='loader'></div></center></section>");
      $(this).addClass("active");
    });
  
    loadModule("order");
  });