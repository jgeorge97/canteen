// Your web app's Firebase configuration
var firebaseConfig = {
//Add Firebase config here
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

var ss = "SESSION";
var session=firebase.auth();
session.onAuthStateChanged(function(user) {
  if (user) {
    window.location='dash.html';
  }
});

/* LOGIN PROCESS */


  function validate(){

    var email = $("#inputEmail").val();
    var password = $("#inputPassword").val();
    if($("#chb").is(":checked"))
      ss = firebase.auth.Auth.Persistence.LOCAL;
    else
      ss = firebase.auth.Auth.Persistence.SESSION;

    if(email != "" && password != ""){
      $("#loginBtn").addClass("disabled");
      $("#loginError").html("");
      
      firebase.auth().setPersistence(ss).then(function() {
        return session.signInWithEmailAndPassword(email, password);
      }).catch(function(error)
      {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;

        $("#loginError").html(errorMessage);
        $("#loginBtn").removeClass("disabled");
      });
    }
    return false;
  }
