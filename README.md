# Canteen Management

A simple web application to order food in a Canteen using Firebase.

## Features

* Order a variety of foods
* Food can be ordered in multiple quantities (now limited to 4 units)
* Cancellation of Selected Orders
* Dynamic Pricing
* More Items can be added in a single Order

## Admin Panel (Canteen-Admin)

* Shows the list of ordered items, their quantity along with the customer's name
* Can view the list of registered users
* Add Users
* Reset orders at the end of the day
* To be managed by the canteen staff
* Admin Panel must be hosted as another website

## Installation

* Clone this repo
* Move canteen-admin folder to server root
* Add your Firebase config to dash.js & index.js in canteen/dist/js & dash.js, index.js, users.js, & signup.js in canteen-admin/dist/js.
* Add an admin user using Firebase Authentication
* Done! All set to go!